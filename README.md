Scala & SBT sample from twitter tutorial
========================================

This is 1st stage from tutorial https://twitter.github.io/scala_school/sbt.html

Notes:

* tested on SBT 0.13.7
* uses *.sbt instead of project/build/*.scala (seems to do not work)


Usage:

	sbt test


Resources
---------

* https://twitter.github.io/scala_school/sbt.html
* http://www.scala-sbt.org/0.13.5/docs/Getting-Started/Basic-Def.html#how-build-sbt-defines-settings
* http://stackoverflow.com/questions/18100226/java-lang-noclassdeffounderror-scala-reflect-classmanifest

