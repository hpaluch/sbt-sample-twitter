
name := "sample"

version := "1.0"

// 2.10.4 produces .NoClassDefFoundError: scala/reflect/ClassManifest
scalaVersion := "2.9.3"

libraryDependencies += "org.codehaus.jackson" % "jackson-core-asl" % "1.6.1"

libraryDependencies += "org.scala-tools.testing" % "specs_2.8.0" % "1.6.5" % "test"

